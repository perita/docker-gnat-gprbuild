FROM alpine:edge

LABEL maintainer="Perita Soft - https://www.peritasoft.com/"

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories

RUN apk --update add --no-cache gprbuild musl-dev